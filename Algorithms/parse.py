import pandas as pd
import string
from nltk.corpus import stopwords

class Parse:
    def comment_strip(self,text):
        text = text.lower()
        text = text.replace("\n"," ")
        #remove links
        text = re.sub(r"https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)", "", text)
        text = re.sub(r'(?i)\b((?:https?://|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}     /)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:\'".,<>?«»“”‘’]))','', text)
        text = re.sub(r"(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}", "", text)
        #remove decimal values
        text = re.sub(r"\b\d+\b", "", text)
        #Removes file extensions
        text = re.sub(r"\.jpg", " ", text)
        text = re.sub(r"\.png", " ", text)
        text = re.sub(r"\.gif", " ", text)
        text = re.sub(r"\.bmp", " ", text)
        text = re.sub(r"\.pdf", " ", text)
        text = string.replace("image:"," ")
        #removes categories
        text = re.sub(r"\[?\[user:.*\|", " ", text)
        text = re.sub(r"\[?\[wikipedia:.*\|", " ", text)
        text = re.sub(r"\[?\[special:.*\|", "", text)
        text = re.sub(r"\[?\[category:.*\|", "", text)
        text = string.replace("wp:"," ")
        text = string.replace("file:"," ")
        #remove prefixes
        text = re.sub(r"^(mr|mrs|miss|dr|prof)[\.\s]*", "", text)
        return text

    def comment_substitute(self,text):
        #substitue words
        
        text = string.replace("´","'")
        text = string.replace("—"," ")
        text = string.replace("’", "'")
        text = string.replace("‘", "'")
        text = string.replace("what's", "what is")
        text = re.sub(r"\'s", " ", text)
        text = re.sub(r"\'ve", " have", text)
        text = string.replace("can't", "cannot")
        text = string.replace("n't", " not")
        text = string.replace("i'm", "i am")
        text = string.replace("i’m", "i am")
        text = re.sub(r"\'re", " are", text)
        text = re.sub(r"\'d", " would", text)
        text = re.sub(r"\'ll", " will", text)
        text = string.replace(",", "")
        text = string.replace("–", " ")
        text = string.replace("−", " ")
        text = re.sub(r"\.", " ", text)
        text = string.replace("!", " !")
        text = re.sub(r"\/", " ", text)
        text = string.replace("_", " ")
        text = re.sub(r"\?", " ?", text)
        text = re.sub(r"\^", " ^ ", text)
        text = re.sub(r"\+", " + ", text)
        text = re.sub(r"\-", " - ", text)
        text = re.sub(r"\=", " = ", text)
        text = string.replace(r"#", " # ")
        text = string.replace("'", " ")
        text = string.replace("<3", " love ")
        text = re.sub(r"(\d+)(k)", r"\g<1>000", text)
        text = string.replace(":", " : ")
        text = string.replace(" e g ", " eg ")
        text = string.replace(" b g ", " bg ")
        text = string.replace(" u s ", " american ")
        text = re.sub(r"\0s", "0", text)
        text = string.replace(r"e - mail", "email")
        text = string.replace(r"j k", "jk")
        return text

    def main(self):
        csv_file = pd.read_csv('train.csv',sep=',', header = None,low_memory=False)
        print(csv_file[2][1])
        for row in range (1,len(csv_file)):
            print(csv_file[1][row])
parse = Parse()
parse.main()