import json

import os
from os import path

import nltk
from nltk.corpus import stopwords
from nltk.tokenize import sent_tokenize,word_tokenize
from .wordStrip import WordStrip


class Theorem:
    positive_count = 0
    negative_count = 0
    unique_count = 0
    accuracy = 0

    STOP_WORDS = set(stopwords.words('english'))
    NAME_LIST = open("Algorithms/theorem/data/names.txt", "rt").read().lower().splitlines()

    def __init__(self):
        print("Initiating theorem class")
        self.parseJson()
        self.checkData()



    def main(self):
        self.accuracyChecker(self.test_positive,True)
        self.accuracyChecker(self.test_negative, False)

    def checkData(self):
        print("start check...")
        print("positive_count: " + str(self.positive_count))
        print("negative_count: " + str(self.negative_count))
        pos_cnt = 0
        for word in self.positive_words:
            pos_cnt += self.positive_words[word]
        neg_cnt = 0
        for word in self.negative_words:
            neg_cnt += self.negative_words[word]
        print(pos_cnt == self.positive_count)
        print(neg_cnt == self.negative_count)
        print("end check...")

    def inputController(self):
        dataPress = input("Press [Y] or [y] to insert your own input: ")
        if(dataPress.lower() == "y"):
            dataInput = input("Please write your input...\n")
            print(dataInput)
            tokenized_comment = nltk.word_tokenize(dataInput)
            self.lapaceSmoothingController(tokenized_comment)
        else:
            self.readCsv()
            print("Thankyou for using the appication")

    def lapaceSmoothingController(self, array):
        print("array " + str(array) + "=================")
        negative_points = 1
        positive_points = 1
        strip = WordStrip()
        for word in array:
            word = strip.clean_text(word)
            print("CLEAN ", word)
            if word not in self.STOP_WORDS and word not in self.NAME_LIST:
                print("asdasdasdasd ", type(word))
                positive_points *= self.laplaceSmoothing(word, True)
                negative_points *= self.laplaceSmoothing(word, False)
        print("positive points:" + str(positive_points))
        print("negative points:" + str(negative_points))
        if(positive_points > negative_points):
            print("the data is positive")
            return (str((positive_points/negative_points)),positive_points,negative_points)
        elif(positive_points < negative_points):
            print("the data is negative")
            return (str((negative_points/positive_points)),positive_points,negative_points)
        else:
            print("the data neutral")
            return (str((negative_points/positive_points)),positive_points,negative_points)

    def laplaceSmoothing(self, word, positiveBool):
        wordCount = self.countWords(word, positiveBool)
        if(wordCount != 0):
            return float(wordCount) / self.getTotalWords(positiveBool)
        else:
            return (float(1) / (self.getTotalWords(positiveBool) + self.unique_count))
        return 0

    def countWords(self, word, positiveBool):
        dictionary = {}
        if (positiveBool):
            dictionary = self.positive_words
        else:
            dictionary = self.negative_words
        if word in dictionary:
            return dictionary[word]
        else:
            return 0

    def getTotalWords(self, positiveBool):
        if (positiveBool):
            return self.positive_count
        else:
            return self.negative_count

    def accuracyChecker(self,array,positive_check):
        wordStrip = WordStrip()
        for text in array:
            negative_points = 1
            positive_points = 1
            wordArray = nltk.word_tokenize(text)
            for word in wordArray:
                word = wordStrip.clean_text(word)
                if word not in self.STOP_WORDS and word not in self.NAME_LIST and len(word) > 1:
                    positive_points *= self.laplaceSmoothing(word, True)
                    negative_points *= self.laplaceSmoothing(word, False)
            if(positive_check):
                if(positive_points >= negative_points):
                    self.accuracy +=1
            else:
                if(negative_points > positive_points):
                    self.accuracy +=1


    def calculate_percentage(self):
        percentage = self.accuracy * 100/ (len(self.test_positive) + len(self.test_negative))
        return percentage

    def parseJson(self):
        JSON_FILE = "Algorithms/theorem/data/output/dictionary.json"
        with open(JSON_FILE) as file:
            dict_file = json.load(file)
        self.positive_words = dict_file['Positive Words']
        self.negative_words = dict_file['Negative Words']
        self.positive_count = dict_file['Positive Count']
        self.negative_count = dict_file['Negative Count']
        self.distinct_words = dict_file['Distinct Words']
        self.distinct_count = len(self.distinct_words)
        self.test_positive = dict_file['Test Positive']
        self.test_negative = dict_file['Test Negative']
