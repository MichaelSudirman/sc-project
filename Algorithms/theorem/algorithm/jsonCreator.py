import json

import os
from os import path


class JsonCreator:

    def __init__(self):
        print("Initiating jsonCreator class")
        self.OUTPUT_FILE = os.path.join(
            os.getcwd(), 'data', 'output', 'dictionary.json')

    def create(self, positive, positive_count, negative, negative_count, distinct, test_positive, test_negative):
        array = {'Positive Words': positive, 'Positive Count': positive_count,
                 'Negative Words': negative, 'Negative Count': negative_count, 'Distinct Words': distinct, 'Test Positive': test_positive,
                 'Test Negative': test_negative}
        self.createJsonFile(output=array)
        print("Finished Creating JSON file... Please check data/output/")

    def createJsonFile(self, output):
        with open(self.OUTPUT_FILE, 'w') as json_file:
            json.dump(output, json_file, indent=4)
