from .csvParse import CsvParse
from .wordStrip import WordStrip
from .jsonCreator import JsonCreator

import nltk
from nltk.corpus import stopwords
from nltk.tokenize import sent_tokenize, word_tokenize

from pattern.en import lemma

import os
from os import path

import numpy as np
import random


class Factory:
    csv_file = None
    positive_words = {}
    negative_words = {}
    distinct_words = []
    train_positive = []
    train_negative = []
    test_positive = []
    test_negative = []
    positive_count = 0
    negative_count = 0
    STOP_WORDS = set(stopwords.words('english'))
    NAME_LIST = open(os.path.join(
        os.getcwd(), 'data', 'names.txt'), "rt").read().lower().splitlines()

    def __init__(self):
        print("Initiating factory class")

    def get_toxic_weight(self, row):
        weight = 0
        for col in range(2, 8):
            weight += int(self.csv_file[col][row])
        return weight

    def add_distinct_word(self, word):
        if word not in self.distinct_words:
            self.distinct_words.append(word)

    def add_positive_word(self, word):
        self.positive_count += 1
        if word in self.positive_words:
            self.positive_words[word] += 1
        else:
            self.positive_words[word] = 1

    def add_negative_word(self, word):
        self.negative_count += 1
        if word in self.negative_words:
            self.negative_words[word] += 1
        else:
            self.negative_words[word] = 1

    def checkComment(self, word, toxic_weight):
        if word not in self.STOP_WORDS and word not in self.NAME_LIST and len(word) > 1:
            if word != None:
                if toxic_weight == 0:
                    self.add_positive_word(word)
                else:
                    self.add_negative_word(word)
                self.add_distinct_word(word)

    def separate_toxic(self, row):
        toxic_weight = self.get_toxic_weight(row)
        self.train_positive.append(
            self.csv_file[1][row]) if toxic_weight == 0 else self.train_negative.append(self.csv_file[1][row])
        if row % 1000 == 0:
            print("Current progress: {0} rows...".format(str(row)))

    def trainer_split(self):
        parsed_positive = []
        for i in range(16225):
            num = random.randint(0, len(self.train_positive))
            parsed_positive.append(self.train_positive.pop(num))

        self.train_positive = parsed_positive[:10500]
        self.test_positive = parsed_positive[10501:]
        temporary_neg = self.train_negative[:12890]
        self.test_negative = self.train_negative[12891:]
        self.train_negative = temporary_neg

    def main(self):
        csv_parse = CsvParse()
        word_strip = WordStrip()
        jsonCreator = JsonCreator()
        self.csv_file = csv_parse.read()
        comments_line = self.csv_file[1]
        cnt = 0
        x = lemma("hello")
        for row in range(1, len(self.csv_file)):
            self.separate_toxic(row)
        print(len(self.train_positive))
        self.trainer_split()
        print(len(self.train_positive))
        print(len(self.train_negative))

        for comm in range(0, len(self.train_positive)):
            comments = word_strip.clean_text(self.train_positive[comm])
            tokenized_comment = nltk.word_tokenize(comments)
            for word in tokenized_comment:
                word = lemma(word)
                self.checkComment(word, 0)
            cnt += 1
            if comm % 1000 == 0:
                print(
                    "Current progress(positive): {0} rows...".format(str(comm)))

        cnt = 0
        for comm in range(0, len(self.train_negative)):
            comments = word_strip.clean_text(self.train_negative[comm])
            tokenized_comment = nltk.word_tokenize(comments)
            for word in tokenized_comment:
                word = lemma(word)
                self.checkComment(word, 1)
            cnt += 1
            if comm % 1000 == 0:
                print(
                    "Current progress(negative): {0} rows...".format(str(comm)))

        jsonCreator.create(self.positive_words, self.positive_count,
                           self.negative_words, self.negative_count, self.distinct_words, self.test_positive, self.test_negative)
        print(self.positive_count)
        print(self.negative_count)
