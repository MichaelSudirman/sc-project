import string
import re


class WordStrip:

    def __init__(self):
        print("Initiating wordStrip class")

    def comment_strip(self, text):
        try:
            text = text.lower()
        except:
            print("Something went wrong... Strange text = {0}".format(text))
            text = str(text)
        text = text.replace("\n", " ")
        # remove links
        text = re.sub(
            r"https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)", "", text)
        text = re.sub(
            r'(?i)\b((?:https?://|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}     /)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:\'".,<>?«»“”‘’]))', '', text)
        text = re.sub(
            r"(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}", "", text)
        # remove decimal values
        text = re.sub(r"\b\d+\b", "", text)
        # Removes file extensions
        text = re.sub(r"\.jpg", " ", text)
        text = re.sub(r"\.png", " ", text)
        text = re.sub(r"\.gif", " ", text)
        text = re.sub(r"\.bmp", " ", text)
        text = re.sub(r"\.pdf", " ", text)
        text = text.replace("image:", " ")
        # removes categories
        text = re.sub(r"\[?\[user:.*\|", " ", text)
        text = re.sub(r"\[?\[wikipedia:.*\|", " ", text)
        text = re.sub(r"\[?\[special:.*\|", "", text)
        text = re.sub(r"\[?\[category:.*\|", "", text)
        text = text.replace("wp:", " ")
        text = text.replace("file:", " ")
        # remove prefixes
        text = re.sub(r"^(mr|mrs|miss|dr|prof)[\.\s]*", "", text)
        return text

    def comment_substitute(self, text):
        # substitue words
        text = text.replace("´", "'")
        text = text.replace("—", " ")
        text = text.replace("’", "'")
        text = text.replace("‘", "'")
        text = text.replace("what's", "what is")
        text = re.sub(r"\'s", " ", text)
        text = re.sub(r"\'ve", " have", text)
        text = text.replace("can't", "cannot")
        text = text.replace("n't", " not")
        text = text.replace("i'm", "i am")
        text = text.replace("i’m", "i am")
        text = re.sub(r"\'re", " are", text)
        text = re.sub(r"\'d", " would", text)
        text = re.sub(r"\'ll", " will", text)
        text = text.replace(",", "")
        text = text.replace("–", " ")
        text = text.replace("−", " ")
        text = re.sub(r"\.", " ", text)
        text = text.replace("!", " !")
        text = re.sub(r"\/", " ", text)
        text = text.replace("_", " ")
        text = re.sub(r"\?", " ?", text)
        text = re.sub(r"\^", " ^ ", text)
        text = re.sub(r"\+", " + ", text)
        text = re.sub(r"\-", " - ", text)
        text = re.sub(r"\=", " = ", text)
        text = text.replace("#", " # ")
        text = text.replace("'", " ")
        text = text.replace("<3", " love ")
        text = re.sub(r"(\d+)(k)", r"\g<1>000", text)
        text = text.replace(":", " : ")
        text = text.replace("e g", " eg ")
        text = text.replace("b g", " bg ")
        text = text.replace("u s", " american ")
        text = re.sub(r"\0s", "0", text)
        text = text.replace("e - mail", "email")
        text = text.replace("j k", "jk")
        return text

    def remove_punctuations(self, text):
        return re.sub(r'[^ A-Za-z0-9]+', '', text)

    def clean_text(self, text):
        text = self.comment_strip(text)
        text = self.comment_substitute(text)
        text = self.remove_punctuations(text)
        return text
