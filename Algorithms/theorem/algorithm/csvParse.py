import pandas as pd

import os
from os import path


class CsvParse:

    def __init__(self):
        print("Initiating csvParse class")
        # self.INPUT_FILE = '../data/input/train_lightest.csv'
        self.INPUT_FILE = os.path.join(os.getcwd(), 'data', 'input','train.csv')

    def __str__(self):
        print("This is a csvParse class")

    def read(self):
        csv_file=pd.read_csv(self.INPUT_FILE, sep = ',',
                               header = None, low_memory = False)
        return csv_file
