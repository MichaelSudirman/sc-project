import json

import os
from os import path

import nltk
from nltk.corpus import stopwords

class ToxicDictionary:

    def __init__(self):
        print("Initiating factory class")
        self.csv_file = None
        self.positive_words = {}
        self.negative_words = {}
        self.distinct_words = []
        self.positive_count = 0
        self.negative_count = 0
        self.stop_words = set(stopwords.words('english'))
        self.NAME_LIST = open(os.path.join(
            os.getcwd(), 'data', 'names.txt'), "rt").read().lower().splitlines()
        self.json = parseJson()

    def parseJson(self):
        print(json.loads())


