from algorithm.factory import Factory
from algorithm.theorem import Theorem


class Main:

    def main(self):
        dataPress = input(
            "Press [D] or [d] for dictionary creation. Press [I] or [i] for input theorem: ")
        if(dataPress.lower() == "d"):
            factory = Factory()
            factory.main()
        if(dataPress.lower() == "i"):
            theorem = Theorem()
            theorem.main()


if __name__ == "__main__":
    Main().main()
