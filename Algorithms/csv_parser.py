import csv
from nltk.corpus import stopwords
import pandas as pd
import nltk
import re
# import pattern
# from pattern.en import lemma
from nltk.tokenize import sent_tokenize,word_tokenize
from nltk.stem import WordNetLemmatizer,SnowballStemmer


class CsvParser:

    def __init__(self):
        self.positive_dictionary = {}
        self.negative_dictionary = {}
        self.distinct_words = {}
        self.count_positive = 0
        self.count_negative = 0
        self.stop_words = set(stopwords.words('english'))
        self.name_list = open("names.txt", "rt").read().lower().splitlines()

    def comment_strip(self,text):
        text = text.lower()
        text = text.replace("\n"," ")
        #remove links
        text = re.sub(r"https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)", "", text)
        text = re.sub(r'(?i)\b((?:https?://|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}     /)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:\'".,<>?«»“”‘’]))','', text)
        text = re.sub(r"(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}", "", text)
        #remove decimal values
        text = re.sub(r"\b\d+\b", "", text)
        #Removes file extensions
        text = re.sub(r"\.jpg", " ", text)
        text = re.sub(r"\.png", " ", text)
        text = re.sub(r"\.gif", " ", text)
        text = re.sub(r"\.bmp", " ", text)
        text = re.sub(r"\.pdf", " ", text)
        text = re.sub(r"image:", " ", text)
        #removes categories
        text = re.sub(r"\[?\[user:.*\|", " ", text)
        text = re.sub(r"\[?\[wikipedia:.*\|", " ", text)
        text = re.sub(r"\[?\[special:.*\|", "", text)
        text = re.sub(r"\[?\[category:.*\|", "", text)
        text = re.sub(r"wp:", " ", text)
        text = re.sub(r"file:", " ", text)
        #remove prefixes
        text = re.sub(r"^(mr|mrs|miss|dr|prof)[\.\s]*", "", text)

        return text

    def comment_substitute(self,text):
        #substitue words
        text = re.sub(r"´", "'", text)
        text = re.sub(r"—", " ", text)
        text = re.sub(r"’", "'", text)
        text = re.sub(r"‘", "'", text)
        text = re.sub(r"what's", "what is", text)
        text = re.sub(r"\'s", " ", text)
        text = re.sub(r"\'ve", " have", text)
        text = re.sub(r"can't", "cannot", text)
        text = re.sub(r"n't", " not", text)
        text = re.sub(r"i'm", "i am", text)
        text = re.sub(r"i’m", "i am", text)
        text = re.sub(r"\'re", " are", text)
        text = re.sub(r"\'d", " would", text)
        text = re.sub(r"\'ll", " will", text)
        text = re.sub(r",", "", text)
        text = re.sub(r"–", " ", text)
        text = re.sub(r"−", " ", text)
        text = re.sub(r"\.", " ", text)
        text = re.sub(r"!", " !", text)
        text = re.sub(r"\/", " ", text)
        text = re.sub(r"_", " ", text)
        text = re.sub(r"\?", " ?", text)
        text = re.sub(r"\^", " ^ ", text)
        text = re.sub(r"\+", " + ", text)
        text = re.sub(r"\-", " - ", text)
        text = re.sub(r"\=", " = ", text)
        text = re.sub(r"#", " # ", text)
        text = re.sub(r"'", " ", text)
        text = re.sub(r"<3", " love ", text)
        text = re.sub(r"(\d+)(k)", r"\g<1>000", text)
        text = re.sub(r":", " : ", text)
        text = re.sub(r" e g ", " eg ", text)
        text = re.sub(r" b g ", " bg ", text)
        text = re.sub(r" u s ", " american ", text)
        text = re.sub(r"\0s", "0", text)
        text = re.sub(r"e - mail", "email", text)
        text = re.sub(r"j k", "jk", text)

        return text

    def remove_punctuations(self,text):
        return re.sub('[^ A-Za-z0-9]+', '', text)

    def clean_text(self, text):
        text = self.comment_strip(text)
        text = self.comment_substitute(text)
        text = self.remove_punctuations(text)
        return text

    def main_parser(self):
        # with open('train.csv',encoding="utf8") as csv_file:
        #     csv_reader = csv.reader(csv_file, delimiter=',')
        #     line_count = 0
        #     for row in csv_reader:
        #         text = row[1]
        #         toxic_indicate = row[2] + row[3] + row[4] + row[5] + row[6] + row[7]
        #         if line_count > 0:
        #             text = self.clean_text(text)
        #             tokenized = nltk.word_tokenize(text)
        #             for word in tokenized:
        #                 if word not in self.stop_words and word not in self.name_list:
        #                         if len(word) > 1:
        #                             text = lemma(word)
        #                             if toxic_indicate == "000000":
        #                                 self.count_positive += 1
        #                             else:
        #                                 self.count_negative += 1
        #
        #
        #         if line_count == 159572:
        #             csv_file.close()
        #             break
        #         print("ROW: " +str(line_count))
        #         print( "pos " + str(self.count_positive))
        #         print("neg " + str(self.count_negative))
        #         print("===============================")
        #
        #         line_count += 1



            csv_file = pd.read_csv('train.csv',sep=',', header = None,low_memory=False)
            print(csv_file[2][1])
            for row in range (1,len(csv_file)):
                comment = csv_file[1][row]
                comment = self.clean_text(comment)
                tokenized_comment = nltk.word_tokenize(comment)
                toxic_indicate = csv_file[2][row] + csv_file[3][row] + csv_file[4][row] + csv_file[5][row] + csv_file[6][row] + csv_file[7][row]
                # print(toxic_indicate)
                for word in tokenized_comment:
                    if word not in self.stop_words and word not in self.name_list:
                        if len(word) > 1:
                            # lemma_word = lemma(word)
                            if toxic_indicate == "000000":
                                self.count_positive +=1
                            else:
                                self.count_negative +=1
            print("ROW: " +str(row))
            print( "pos " + str(self.count_positive))
            print("neg " + str(self.count_negative))
            print("===============================")


parse = CsvParser()
parse.main_parser()
print(parse.count_negative)
print(parse.count_positive)