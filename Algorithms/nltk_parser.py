import nltk
import re
from nltk.corpus import movie_reviews,stopwords
from nltk.tokenize import sent_tokenize,word_tokenize

class NLTK_PARSER:
    def __init__(self):
        pass

    def strip_punc(self,s):

        return re.sub('\W+',' ', s)

    def movie(self):
        docs = [(list(movie_reviews.words(fileID)), category)
                for category in movie_reviews.categories()
                for fileID in movie_reviews.fileids(category)]

        # for i in docs:
        #     print(i)
        return docs

    def movie_strip(self,movlist):
        stop_words = set(stopwords.words('english'))
        temp = []
        name_list = open("names.txt","rt").read().lower().splitlines()
        for i in range (len(movlist)):
            word = self.strip_punc(movlist[i]).lower()
            if(word !=  " "):
                if word not in stop_words and word not in name_list:
                    temp.append(word)
        return temp

    # def neg_movie():
    #     neg_reviews = []
    #     for fileid in movie_reviews.fileids('neg'):
    #         words = movie_reviews.words(fileid)
    #         for i in words:
    #             print(i)

    def array_loop(self,arr):
        positive = []
        negative = []
        for i in range (len(arr)):
            if arr[i][1] == "pos":
               positive.append((self.create_word_features(self.movie_strip(arr[i][0])), "positive"))
            else:
                negative.append((self.create_word_features(self.movie_strip(arr[i][0])), "negative"))
        return (positive, negative)

    def main(self):
        # stop_words = set(stopwords.words('english'))
        #         # name_list = open("names.txt", "rt").read().lower().splitlines()
        #         # movie_list = self.movie()
        #         # negative_words = self.movie_strip(movie_list[0][0])
        #         # positive_words = self.movie_strip(movie_list[1][0])
        #         # # arrNegative = [negative_words]
        #         # # arrPositive =[positive_words,movie()[1][1]]
        #         # pos_review = [self.create_word_features(positive_words),'positive']
        #         # neg_review = [self.create_word_features(negative_words),'negative']
        # rint(pos_review)p
        pos,neg = self.array_loop(self.movie())
        print(pos)
        print("+++++++++++++++++++++++++++")
        print(neg)


    def create_word_features(self,wordlist):
        dictio = dict([(word, True) for word in wordlist])
        return dictio



    # for category in movie_reviews.categories():
    #     for fileID in movie_reviews.fileids(category):
    #         print(movie_reviews.words(fileID))

    # ex = "hello Andre, how are you?"
    # # print(word_tokenize(strip_punc(ex)))

parse= NLTK_PARSER()
parse.main()



