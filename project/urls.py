from django.urls import path
from . import views

urlpatterns = [
    path('', views.main, name='main'),
    path('input',views.input,name='input'),
    path('about', views.about, name='about')
]