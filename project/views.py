from django.shortcuts import render
from Algorithms.theorem.algorithm.theorem import Theorem
from Algorithms.theorem.algorithm.wordStrip import WordStrip
from nltk.tokenize import sent_tokenize, word_tokenize
import nltk


def main(request):
    return render(request, "main.html")

def about(request):
    return render(request, "about.html")

def input(request):
    message = request.POST.get('message')
    output=""
    percentage = "-"
    positive_point=  "-"
    negative_point= "-"
    bool=None
    check_pos= ""
    val_pos = 0
    val_neg = 0
    if message != None:
        print(message)
        theorem = Theorem()
        tokenized = nltk.word_tokenize(message)
        print("TOKENIZED " + str(tokenized))
        percentage,val_pos,val_neg = theorem.lapaceSmoothingController(tokenized)
        positive_point = str(val_pos)
        negative_point = str(val_neg)
        output=message

        if val_pos > val_neg:
            check_pos = "Your comment is positive."
            bool = True
        elif val_neg > val_pos:
            check_pos = "Your comment is fairly toxic."
            bool= False
        else:
            check_pos = "Your comment is neutral."


    context = {'output': output ,'percentage':percentage,'bool':bool,'positive_point':positive_point,'negative_point':negative_point,"check_pos":check_pos}
    return render(request, "input_page.html",context)